// @flow
import { Apis } from 'graphenejs-ws';
import { ChainStore } from 'graphenejs-lib';
import React, { Component } from 'react';
import styles from './Home.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connectedToBlockchain: false,
      disableConnectButton: false,
    };
    this.connectToBlockchain = this.connectToBlockchain.bind(this);
    this.getObject = this.getObject.bind(this);
  }

  getObject() {
    const object = ChainStore.getObject('2.1.0');
    console.log('ChainStore object 2.1.0:\n', object ? object.toJS() : object);
  }

  connectToBlockchain() {
    // Disable connect button
    this.setState({ disableConnectButton: true });
    // Open websocket connection
    Apis.instance('wss://peerplays-dev.blocktrades.info/ws', true).init_promise.then((res) => {
      console.log('Connected to:', res[0].network);
      // Init chainstore
      ChainStore.init().then(() => {
        // Mark connected to blockchain
        this.setState({ connectedToBlockchain: true });
        console.log('Chainstore init success');
        return null;
      }).catch((error) => {
        console.log('Fail to init ChainStore');
        console.error(error);
        this.setState({ disableConnectButton: false });
      });
      return null;
    }).catch((error) => {
      console.log('Fail to connect to blockchain');
      console.error(error);
      this.setState({ disableConnectButton: false });
    });
  }

  render() {
    return (
      <div>
        <div className={styles.container}>
          <h2>Home</h2>
          <button onClick={this.connectToBlockchain} disabled={this.state.disableConnectButton}>
            {!this.state.connectedToBlockchain ? 'Connect to Blockchain' : 'Connected'}
          </button>
          <button onClick={this.getObject} disabled={!this.state.connectedToBlockchain}>
            Get object
          </button>
        </div>
      </div>
    );
  }
}

export default Home;
